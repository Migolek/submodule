export const state = {
  test: false,
}

export const getters = {
  test(state) {
    return state.test
  },
}

export const mutations = {
  SET_TEST(state, payload) {
    state.test = payload
  },
}

export const actions = {
  setTest({ commit }, statement) {
    commit('SET_TEST', statement)
  },
}
